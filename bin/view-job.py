#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/view-job.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
parser = argparse.ArgumentParser(description='View details of a job ID.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
parser.add_argument('jobids', action='store', nargs='+', default=None, help='Job IDs to cancel.')
args = parser.parse_args()
#
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
client = clientclass.connect()
jobclass = bp.jobstats.JobStats(debug=args.debug, loglevel=args.loglevel)
rptclass = bp.report.Report(debug=args.debug, loglevel=args.loglevel)
#
for jobid in args.jobids:
    jobinfo = client.get_job_spectra_s3(ds3.GetJobSpectraS3Request(jobid))
    #clientclass.dump_dict('ds3.GetJobSpectraS3Request.json', jobinfo.result)
    jobclass.build_job_stats(jobinfo.result)
    print('{}'.format(rptclass.job_detail(jobid, jobclass, gb=True)))
    
