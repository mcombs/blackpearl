#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/cancel-activejob.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
#-----------------------------------------------------------------------------#
# Parse arguments and set up logging.
parser = argparse.ArgumentParser(description='Cancel job ID on the Blackpearl.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--force', action='store_true', default=False, help='Do not prompt for confirmation.')
parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
parser.add_argument('--jobtype', action='store', default=None, choices=['GET', 'PUT'], help='Log level.')
parser.add_argument('jobids', action='store', nargs='+', default=None, help='Job IDs to cancel.')
args = parser.parse_args()
#
#
if not args.jobtype and not args.jobids:
    sys.stderr.write('Neither job type nor job ID specified.  Nothing to do.\n')
    sys.exit(0)
if args.jobtype and args.jobids:
    raise Exception('Options --jobtype and --jobids are mutually exclusive.')
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
#-----------------------------------------------------------------------------#
# Set up classes and stats about active jobs.
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
client = clientclass.connect()
rptclass = bp.report.Report(debug=args.debug, loglevel=args.loglevel)
#
ajs = bp.activejobs.ActiveJobs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
ajs.build_active_jobs()
#
#-----------------------------------------------------------------------------#
def proceed(jid=None):
    valid = {'YES': True, 'NO': False, 'N': False}
    while True:
        sys.stdout.write('Cancel job ID:\n    {}\n    [YES / NO] (Default: NO) : '.format(jid))
        choice = raw_input()
        if choice == '':
            return False
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write('Please respond with "YES" or "NO" ')
    return False
#
#-----------------------------------------------------------------------------#
jobids = args.jobids
for jobid in jobids:
    if jobid not in ajs.JobIds():
        raise Exception('Job ID "{}" not present in list of active job IDs "{}".'.format(jobid, ajs.JobIds()))
#
for jobid in list(jobids):
    if args.force == False:
        if proceed(jobid): continue
    log.warning('Cancelling job ID "{}" : \n{}'.format(jobid, rptclass.active_job(jobid, ajs)))
    cancellations = client.cancel_active_job_spectra_s3(ds3.CancelActiveJobSpectraS3Request(jobid))
    clientclass.log_api_call(cancellations)
sys.exit()
