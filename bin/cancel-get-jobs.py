#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/list-activejobs.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
parser = argparse.ArgumentParser(description='Cancel all GET jobs.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
# Client class use to cancel the jobs.
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
client = clientclass.connect()
#
# Build a list of active jobs.
ajs = bp.activejobs.ActiveJobs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
ajs.build_active_jobs()
#
# Cancel the active jobs.
activejobstats = ajs.activejobs
for jobid in sorted(activejobstats.keys()):
    if ajs.RequestType(jobid) == 'GET':
        log.info('Cancelling GET job with jobid "{}".'.format(jobid))
        client.cancel_active_job_spectra_s3(ds3.CancelActiveJobSpectraS3Request(jobid))
