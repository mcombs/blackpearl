#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/list-active-jobs.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
#-----------------------------------------------------------------------------#
# Parse arguments and set up logging.
parser = argparse.ArgumentParser(description='List active jobs.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
parser.add_argument('--gb', action='store_true', default=False, help='Print in gigabytes instead of bytes.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
#-----------------------------------------------------------------------------#
# Set up classes and stats about active jobs.
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
ajs = bp.activejobs.ActiveJobs(debug=args.debug, loglevel=args.loglevel)
jobclass = bp.jobstats.JobStats(debug=args.debug, loglevel=args.loglevel)
rptclass = bp.report.Report(debug=args.debug, loglevel=args.loglevel)
ajs.build_active_jobs()
print('{}'.format(rptclass.all_active_jobs(ajs, gb=True)))
