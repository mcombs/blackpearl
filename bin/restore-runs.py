#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/restore-runs.py
#=============================================================================#
#
import sys
import argparse
import time
import bp
parser = argparse.ArgumentParser(description='Restore the specified run IDs.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('runids', action='store', nargs='+', default=None, help='RUs/RDs to restore.')
args = parser.parse_args()
if args.debug == True: args.loglevel='DEBUG'
#
# Set up needed classes.
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
objectclass = bp.objs.Objs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
restclass = bp.restore.Restore(debug=args.debug, loglevel=args.loglevel, client_class=clientclass, object_class=objectclass)
rptclass = bp.report.Report(debug=args.debug, loglevel=args.loglevel)
#
log.info('Examining tapes for {} runs.'.format(len(args.runids)))
log.debug('Examining tapes for runs "{}".'.format(args.runids))
objectclass.build_object_stats(wanted=args.runids)
runids_on_tape = []
for runid in objectclass.stats(args.runids):
    runids_on_tape.append(runid)
#
missing = []
for rid in args.runids:
    if rid not in runids_on_tape:
        missing.append(rid)
#
if len(missing) > 0:
    log.warning('The following run IDs were not found on tape : {}'.format(missing))
elif len(runids_on_tape) == 0:
    log.warning('No run IDs found on tape.  Nothing to recall.')
    sys.exit(1)
#
#
restclass.GB(True)
log.info('Runs on tape / runs requested : {}/{}'.format(len(runids_on_tape), len(args.runids)))
jobid = restclass.pull_from_tape(runids_on_tape)
log.info('Generated job ID "{}".'.format(jobid))
#
# Now monitor the Blackpearl for the job.
msg = 'Monitoring Blackpearl cache and downloading runs.\n'
msg += '    {:<30s} : {}\n'.format('Run IDs',args.runids)
msg += '    {:<30s} : {}\n'.format('Download Location',restclass.RestoreDir())
log.info(msg)
restclass.monitor()
restclass.move_to_restoredir()
sys.exit(0)
