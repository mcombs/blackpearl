#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/client.py
#=============================================================================#
#
import os
import sys
import json
from ds3 import ds3
from . import log, conf
#
#=============================================================================#
class Client(object):
    '''DS3 clients.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        connect                : Return a client to the Blackpearl.
        log_api_call           : Log a call to the Blackpearl.
        dump_result            : Format the result from a Blackpearl query
                                 in JSON format for troubleshooting.
    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal variables.
        self.client = None
        return
    def connect(self):
        self.log.info('Establishing connection with the BlackPearl.')
        myconf = conf.Conf(debug=self.debug, loglevel=self.loglevel)
        cfg = myconf.read_config()
        self.client = ds3.Client(cfg['blackpearl_ip'], ds3.Credentials(cfg['access_key'], cfg['secret_key']))
        return self.client
    def log_api_call(self, r):
        if not r: return
        self.log.debug('''
======================================== API Call ========================================
Request          : {}
Response         : {}
Result           : {}
==========================================================================================
        '''.format(r.request, r.response, r.result))
    def dump_dict(self, name=None, dictionary=None, location=True):
        if not name and not dictionary: return
        printout = '{}\n"{}"\n'.format('='*77, name)
        printout += '{}'.format(json.dumps(dictionary, indent=4))
        if location == 'file':
            fd = open(name, 'w')
            fd.write(printout)
            fd.close()
        elif location == 'stdout':
            sys.stdout.write('{}'.format(printout))
        else:
            sys.stderr.write('{}'.format(printout))
        return
#
#
#=============================================================================#
