#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/timestats.py
#=============================================================================#
#
import sys
import os
import re
import time
import datetime
from . import log
#
#=============================================================================#
class TimeStats(object):
    '''Common time methods.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        method_name            : Method description.

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.regex_date = re.compile(r'.*(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z)')
        return
    def utctime(self):
        ' 2019-07-31T20:10:36.000Z '
        t = datetime.datetime.utcfromtimestamp(time.time())
        t2 = t.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
        return str(t2 + 'Z')
    def pull_date(self, datestring=None):
        match = re.search(self.regex_date, datestring)
        if match:
            return datetime.strptime(match.group(1), '%Y-%m-%dT%H:%M:%S.%fZ')
        else:
            return None
#
#=============================================================================#
