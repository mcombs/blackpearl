#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/restorestats.py
#=============================================================================#
#
import sys
import os
import re
import tempfile
from ds3 import ds3 
from . import log, objs, timestats, jobstats
#
#=============================================================================#
class RestoreStats(object):
    '''Blackpearl restoration methods.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        method_name            : Method description.

    From ds3.get_bulk_job_spectra_s3(ds3.GetBulkJobSpectraS3Request()).result
    {
        "JobId": "43318f08-b366-4617-bee5-ae8d980e8d61",
        "Status": "IN_PROGRESS",
        "StartDate": "2019-06-03T20:43:30.000Z",
        "CachedSizeInBytes": "0",
        "OriginalSizeInBytes": "32705105920",
        ...
        "ObjectsList": [
            {
                "ChunkNumber": "0",
                "ChunkId": "a297442f-e39a-4df9-8349-c0569028b05a",
                "ObjectList": [
                    {
                        "VersionId": "7d80f8c1-7896-4d56-9e17-d701cab75fd6",
                        "Name": "invitae.komprise.com/kc/.86946/data/86935/run_data__11000__11007.tar",
                        "Offset": "0",
                        "Id": "33ee7e79-6855-4404-94d8-22b38ea51e9a",
                        "Length": "13297295360",
                        "InCache": "true"
                    }
                ]
            }
        ],
    }

    '''
    def __init__(self, debug=False, loglevel='INFO', object_class=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal classes.
        if not isinstance(object_class, objs.Objs):
            raise Exception('Missing class "objs.Objs".  Received "{}".'.format(type(object_class)))
        self.objectclass = object_class
        self.timeclass = timestats.TimeStats(debug=self.debug, loglevel=self.loglevel)
        self.jobstatsclass = jobstats.JobStats(debug=self.debug, loglevel=self.loglevel)
        #
        # Internal variables.
        self.restorestats = {}
        self.init()
        return
    #----------------------------------------------------------------------#
    def init(self):
        self.restorestats = {
            'RequestTime'              : 'Not set',
            'DownloadStartTime'        : 'Not set',
            'DownloadCompleteTime'     : 'Not set',
            'RestoreTime'              : 'Not set',
            'JobId'                    : None,
            'ChunksForDownload'        : [],
            'ChunksDownloaded'         : [],
        }
        return
    def RequestTime(self, settime=None):
        if settime == 'set':
            self.restorestats['RequestTime'] = self.timeclass.utctime()
        return self.restorestats['RequestTime']
    def DownloadStartTime(self, settime=None):
        if settime == 'set':
            self.restorestats['DownloadStartTime'] = self.timeclass.utctime()
        return self.restorestats['DownloadStartTime']
    def DownloadCompleteTime(self, settime=None):
        if settime == 'set':
            self.restorestats['DownloadCompleteTime'] = self.timeclass.utctime()
        return self.restorestats['DownloadCompleteTime']
    def RestoreTime(self, settime=None):
        if settime == 'set':
            self.restorestats['RestoreTime'] = self.timeclass.utctime()
        return self.restorestats['RestoreTime']
    def ChunksForDownload(self):  return self.restorestats['ChunksForDownload']
    def ChunksDownloaded(self):   return self.restorestats['ChunksDownloaded']
    def JobId(self, jobid=None):
        if jobid: self.restorestats['JobId'] = str(jobid)
        return self.restorestats['JobId']
    #
    #----------------------------------------------------------------------#
    def dowloaded_chunk(self, chunkid=None):
        if chunkid and chunkid in self.ChunksForDownload() and chunkid not in self.ChunksDownloaded():
            self.restorestats['ChunksForDownload'].remove(chunkid)
            self.restorestats['ChunksDownloaded'].append(chunkid)
        return
    def build_restore_stats(self, result=None):
        jobid = self.jobstatsclass.build_job_stats(result)
        self.RequestTime('set')
        self.restorestats['JobId'] = jobid
        self.restorestats['ChunksForDownload'] = self.JobChunkIds(jobid)
        return jobid
    #
    #----------------------------------------------------------------------#
    def JobStatus(self, jobid):              return self.jobstatsclass.JobStatus(jobid)
    def JobRequestType(self, jobid):         return self.jobstatsclass.JobRequestType(jobid)
    def JobStartDate(self, jobid):           return self.jobstatsclass.JobStartDate(jobid)
    def JobCachedSizeInBytes(self, jobid):   return self.jobstatsclass.JobCachedSizeInBytes(jobid)
    def JobOriginalSizeInBytes(self, jobid): return self.jobstatsclass.JobOriginalSizeInBytes(jobid)
    def JobChunkIds(self, jobid):            return self.jobstatsclass.JobChunkIds(jobid)
    def JobRunIds(self, jobid):              return self.jobstatsclass.JobRunIds(jobid)
    def ChunkJobId(self, chunkid):           return self.jobstatsclass.ChunkJobId(chunkid)
    def ChunkRunIds(self, chunkid):          return self.jobstatsclass.ChunkRunIds(chunkid)
    def ChunkSubObjects(self, chunkid):      return self.jobstatsclass.ChunkSubObjects(chunkid)
    def ChunkSize(self, chunkid):            return self.jobstatsclass.ChunkSize(chunkid)
    def SubChunkRunId(self, subchunkid):     return self.jobstatsclass.SubChunkRunId(subchunkid)
    def SubChunkInCache(self, subchunkid):   return bool(self.jobstatsclass.SubChunkInCache(subchunkid))
    def SubChunkOffset(self, subchunkid):    return self.jobstatsclass.SubChunkOffset(subchunkid)
    def SubChunkLength(self, subchunkid):    return self.jobstatsclass.SubChunkLength(subchunkid)
#
#=============================================================================#
