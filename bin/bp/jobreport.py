#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/jobreport.py
#=============================================================================#
#
import sys
import os
from . import log, timestats, objs
#
#=============================================================================#
class JobReport(object):
    '''Blackpearl restoration methods.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        method_name            : Method description.

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        return
    #----------------------------------------------------------------------#
    def report(self):
        rpt = 'Job Report :\n'
        rpt += 'Job\n'
        rpt += '  {:<30} : {:<}\n'.format('Job Id', self.jobid)
        rpt += '  {:<30} : {:<}\n'.format('Report Timestamp', self.timestats.utctime)
        rpt += '  {:<30} : {:<}\n'.format('Status', self.jobstats['Status'])
        rpt += '  {:<30} : {:<}\n'.format('Bytes in Cache', self.jobstats['cachedsize'])
        rpt += '  {:<30} : {:<}\n'.format('Chunk IDs', self.jobstats['chunkids'])
        rpt += '\nChunks\n'
        rpt += '  {:<30}  {:<15}  {:<}\n'.format('Chunk ID', 'Chunk Size', 'Run IDs')
        for chunkid in self.chunkstats.keys():
            rpt += '  {:<30}  {:<15}  {:<}\n'.format(chunkid, self.chunkstats['size'], self.chunkstats['runids'])
        return
#
#=============================================================================#
