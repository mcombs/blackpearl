#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/objs.py
#=============================================================================#
#
import sys
import os
import re
import json
from ds3 import ds3 
from . import log, client
#
#=============================================================================#
class Objs(object):
    '''Blackpearl object data management.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        stats                  : The entire object or part of the object
                                 dictionary.
        is_ru                  : Does the object name contain an RU tar archive?
        get_runid              : RunID with leading 'RU/RD' for an object.
        BucketName             : Bucket name.
        ObjectName             : Full object name.
        Basename               : Basename for the object.
        Size                   : Object size.
        LastModified           : Datetime last modified.
        TapeInfo               : List of tape information of 'BarCode' and
                                 'SerialNumber'.
        build_object_stats     : Build stats of objects on tape.
        
    '''
    def __init__(self, debug=False, loglevel='WARNING', client_class=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        self.bucket_name = 'komprise_data'
        self.regex_rundata = re.compile(r'.*run_data_{1,2}[0-9]+__(R?[UD]?[0-9]+)\.tar')
        self.regex_numonly = re.compile(r'.*run_data_{1,2}[0-9]+__([0-9]+)\.tar')
        #
        # Internal classes.
        if not client_class:
            self.clientclass = client.Client(debug=False, loglevel='WARNING')
        elif not isinstance(client_class, client.Client):
            raise Exception('Missing class "client.Client".  Received "{}".'.format(type(client_class)))
        else:
            self.clientclass = client_class
        self.client = self.clientclass.connect()
        self.objstats = {}
        self.retrieved = 0
        self.initialized = 0
        return
    #----------------------------------------------------------------------#
    # Variable specific methods.
    def stats(self, wanted=None):
        self.log.debug('Pulling object statistics with restrictions "{}".'.format(wanted))
        if wanted:
            subdict = {}
            for runid in self.objstats.keys():
                if runid in wanted:
                    subdict[runid] = self.objstats[runid]
            return subdict
        return self.objstats
    def is_ru(self, object_name):
        if re.match(self.regex_rundata, object_name): return True
        return False
    def get_runid(self, name):
        m = str(re.match(self.regex_rundata, name).group(1))
        if self._num_only(name):
            return('RU' + m)
        return m
    def _num_only(self, name):
        bn = os.path.basename(name)
        if re.match(self.regex_numonly, name): return True
        return False 
    #----------------------------------------------------------------------#
    # Internal dictionaries
    def BucketName(self):                   return self.bucket_name
    def ObjectRunIds(self):                 return self.objstats.keys()
    def ObjectName(self, runid):            return self.objstats[runid]['ObjectName']
    def ObjectBasename(self, runid):        return self.objstats[runid]['Basename']
    def ObjectSize(self, runid):            return self.objstats[runid]['Size']
    def ObjectLastModified(self, runid):    return self.objstats[runid]['LastModified']
    def ObjectTapeInfo(self, runid):        return self.objstats[runid]['TapeInfo']
    def init(self, object_name=None, objects=None):
        runid = self.get_runid(object_name)
        if runid in self.objstats.keys():
            raise Exception('RunID already exists "{}".'.format(runid))
        self.objstats[runid] = {
            'ObjectName'       : object_name,
            'Basename'         : os.path.basename(object_name),
            'Size'             : int(objects['Size']),
            'LastModified'     : str(objects['LastModified']),
            'VersionId'        : str(objects['VersionId']),
            'TapeInfo'         : [],
        }
        if str(objects['VersionId']) == 'null': versionid = None
        return
    #----------------------------------------------------------------------#
    # Object building methods.
    def build_object_stats(self, wanted=None):
        self.log.info('Building object stats for runids with restrictions "{}".'.format(wanted))
        (marker,nextmarker) = self._get_objects(marker=None, wanted=wanted)
        while self._still_looking(nextmarker=nextmarker, wanted=wanted):
            (marker,nextmarker) = self._get_objects(nextmarker, wanted=wanted)
        self._add_tapes()
        return
    def _still_looking(self, nextmarker=None, wanted=None):
        if wanted:
            if nextmarker and len(self.objstats.keys()) < len(wanted): return True 
            else: return False 
        else:
            return nextmarker
    def _get_objects(self, marker=None, wanted=None):
        (retrieved, initialized) = (0,0)
        bucket_contents = self.client.get_bucket(ds3.GetBucketRequest(self.bucket_name, marker=marker))
        for objects in bucket_contents.result['ContentsList']:
            retrieved += 1
            object_name = objects['Key']
            if not self.is_ru(object_name): continue
            if wanted: 
                if self.get_runid(object_name) in wanted:
                    initialized += 1
                    self.init(object_name, objects)
            else:
                initialized += 1
                self.init(object_name, objects)
        self.retrieved += retrieved
        self.initialized += initialized
        self.log.info('Initialized {} of {} objects retrieved.'.format(self.initialized, self.retrieved))
        return (bucket_contents.result['Marker'],bucket_contents.result['NextMarker'])
    def _add_tapes(self):
        self.log.info('Adding tape information.')
        max_objects = 1000
        n = 0
        for runid in self.objstats.keys():
            if n > 0 and n%max_objects == 0:
                self.log.info('Added tape information for {} objects.'.format(n))
            tapes = self.client.get_physical_placement_for_objects_spectra_s3(
                ds3.GetPhysicalPlacementForObjectsSpectraS3Request(
                    bucket_name=self.bucket_name,
                    object_list=[ ds3.Ds3GetObject(self.ObjectName(runid)) ]
                )
            )
            tapelist = []
            for tape in tapes.result['TapeList']:
                tapelist.append(
                    { 'BarCode'      : tape['BarCode'],
                      'SerialNumber' : tape['SerialNumber'] }
                )
            self.objstats[runid]['TapeInfo'] =  tapelist
            n += 1
        self.log.info('Added tape information for {} objects.'.format(n))
        return
#=============================================================================#
