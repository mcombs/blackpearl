#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/subchunkstats.py
#=============================================================================#
#
import sys
import os
from . import log, client, objs
#
#=============================================================================#
class SubChunkStats(object):
    '''Blackpearl job chunk statistics.

    Attributes:
        debug        (OPT: bool)       : Enable debug mode.
        loglevel     (OPT: str)        : Log level.
        object_class (REQ: objs.Objs)  : Blackpearl object class.

    Methods:
        JobId                  : Job ID for a chunk.
        RunIds                 : Run IDs in a chunk.
        SubObjects             : List of sub-object IDs in a chunk.
        Size                   : Chunk size.
        build_chunk_stats      : Parse result from ds3.get_bulk_job_spectra_s3()
                                 to build chunk statistics.

    From ds3.get_bulk_job_spectra_s3(ds3.GetBulkJobSpectraS3Request()).result
    {
        "JobId": "43318f08-b366-4617-bee5-ae8d980e8d61",
        "Status": "IN_PROGRESS",
        "StartDate": "2019-06-03T20:43:30.000Z",
        "CachedSizeInBytes": "0",
        "OriginalSizeInBytes": "32705105920",
        ...
        "ObjectsList": [                                             <-- Plural
            {
                "ChunkNumber": "0",
                "ChunkId": "a297442f-e39a-4df9-8349-c0569028b05a",
                "ObjectList": [                                      <-- Singular
                    {
                        "InCache": "false",
                        "Latest": "true",
                        "Length": "68719476736",
                        "Id": "23857737-b648-41a8-98c6-65c5380e9313",
                        "VersionId": "06b99a6e-4487-4ab7-abf7-e1b8d392c648",
                        "Name": "invitae.komprise.com/kc/.86946/data/86935/run_data_11000__RU11845.tar",
                        "Bucket": null,
                        "Offset": "0",
                        "PhysicalPlacement": null
                    },
                    {
                        "InCache": "false",
                        "Latest": "true",
                        "Length": "68719476736",
                        "Id": "821b1f65-30e0-421d-8caa-e7c4f521c199",
                        "VersionId": "06b99a6e-4487-4ab7-abf7-e1b8d392c648",
                        "Name": "invitae.komprise.com/kc/.86946/data/86935/run_data_11000__RU11845.tar",
                        "Bucket": null,
                        "Offset": "68719476736",
                        "PhysicalPlacement": null
                    },
                ]
            }
        ],
    }

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal classes.
        self.objectclass = objs.Objs(debug=False, loglevel='WARNING')
        self.clientclass = client.Client(debug=False, loglevel='WARNING')
        #
        # Internal variables.
        self.subchunkstats = {}
        return
    #----------------------------------------------------------------------#
    def RunId(self, subchunkid):         return self.subchunkstats[subchunkid]['RunId']
    def InCache(self, subchunkid):       return bool(self.subchunkstats[subchunkid]['InCache'])
    def Offset(self, subchunkid):        return self.subchunkstats[subchunkid]['Offset']
    def Length(self, subchunkid):        return self.subchunkstats[subchunkid]['Length']
    def init(self, subchunkid=None):
        self.log.debug('Initializing subchunk ID "{}".'.format(subchunkid))
        if subchunkid in self.subchunkstats.keys():
            raise Exception('Subchunk ID "{}" already defined.'.format(subchunkid))
        self.subchunkstats[subchunkid] = {
            'RunId'                : '',
            'InCache'              : False,
            'Offset'               : 0,
            'Length'               : 0,
        }
        return
    #----------------------------------------------------------------------#
    def build_subchunk_stats(self, result=None):
        self.log.info('Building subchunk statistics.')
        for chunk in result['ObjectsList']:
            chunkid=str(chunk['ChunkId'])
            for obj in chunk['ObjectList']:
                subchunkid = str(obj['Id'])
                #
                # Skip any chunks which have already been initialized.
                if subchunkid in self.subchunkstats.keys(): continue
                self.init(subchunkid)
                self.subchunkstats[subchunkid]['RunId']       = self.objectclass.get_runid(str(obj['Name']))
                self.subchunkstats[subchunkid]['Length']      = int(obj['Length'])
                self.subchunkstats[subchunkid]['Offset']      = int(obj['Offset'])
                if obj['InCache'] == 'true':
                    self.subchunkstats[subchunkid]['InCache'] = True
        return
#
#=============================================================================#
