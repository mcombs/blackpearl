#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/restore.py
#=============================================================================#
#
import sys
import os
import time
import datetime
import re
import json
import tempfile
from ds3 import ds3 
from . import log, client, objs, jobstats, restorestats, activejobs, report
#
#=============================================================================#
class Restore(object):
    '''Blackpearl restoration methods.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        method_name            : Method description.

    '''
    def __init__(self, debug=False, loglevel='INFO', client_class=None, object_class=None):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Run some tests before starting.
        if not isinstance(client_class, client.Client):
            raise Exception('Missing class "client.Client".  Received "{}".'.format(type(client_class)))
        self.clientclass = client_class
        self.client = client_class.connect()
        if not isinstance(object_class, objs.Objs):
            raise Exception('Missing class "objs.Objs".  Received "{}".'.format(type(object_class)))
        self.objclass = object_class
        #
        # Internal classes.
        self.restorestatsclass = restorestats.RestoreStats(debug=self.debug, loglevel=self.loglevel, object_class=self.objclass)
        self.activejobsclass = activejobs.ActiveJobs(debug=self.debug, loglevel=self.loglevel, client_class=self.clientclass)
        self.rptclass = report.Report(debug=self.debug, loglevel=self.loglevel)
        #
        # Internal variables.
        self.jobid = None
        self.waitmin = 5
        self.restoredir = '/archive/bp_restore'
        self.tempdir = '/archive/bp_restore/tmp'
        self.gb = False
        # Memory for concatenating downloaded chunks into a large file.
        self.memchunks = 1024*1024*64
        self.tempfiles = {}
        self.spectra_object_list = None
        return
    #
    #----------------------------------------------------------------------#
    def JobID(self, jobid=None):
        if jobid: self.jobid = jobid
        return self.jobid
    def Wait(self, waitmin=None):
        if waitmin: self.waitmin = int(waitmin)
        return self.waitmin
    def RestoreDir(self, restoredir=None):
        if restoredir: self.restoredir = str(restoredir)
        return self.restoredir
    def TempDir(self, tempdir=None):
        if tempdir: self.tempdir = str(tempdir)
        return self.tempdir
    def GB(self, value=False):
        if isinstance(value, bool): self.gb=bool(value)
        return self.gb
    def MemChunks(self, value=None):
        if isinstance(value, int): self.memchunks=int(value)
        return self.memchunks
    #
    #----------------------------------------------------------------------#
    def print_report(self):
        rpt = self.rptclass.restore(jobid=self.jobid, restoreclass=self.restorestatsclass, gb=self.gb)
        sys.stderr.write('{}'.format(rpt))
        return
    def pull_from_tape(self, runids=None):
        self.log.info('Initiating retrieval of {} run IDs from tape to Blackpearl cache.'.format(len(runids)))
        spectra_object_list = []
        for runid in runids:
            spectra_object_list.append(ds3.Ds3GetObject(self.objclass.ObjectName(runid)))
        bulk_get_result = self.client.get_bulk_job_spectra_s3(
            ds3.GetBulkJobSpectraS3Request(
                self.objclass.BucketName(), 
                spectra_object_list
            )
        )
        if self.debug:
            self.clientclass.dump_dict( 'ds3.GetBulkJobSpectraS3Request.json', bulk_get_result.result)
        #
        # Only one job ID should exist for a given restore.
        self.jobid = self.restorestatsclass.build_restore_stats(result=bulk_get_result.result)
        self.print_report()
        return self.jobid
    #
    #----------------------------------------------------------------------#
    def monitor(self, jobid=None):
        if jobid:              self.jobid = jobid
        if self.jobid == None: raise Exception('Job ID required.')
        #
        # If we do not have any statistics for our restore built by the
        # look for the job id amongst active jobs.
        if self.restorestatsclass.JobId() == None:
            self._build_from_active_jobs()
        while len(self.restorestatsclass.ChunksForDownload()) > 0:
            self.print_report()
            chunkquery = self.objclass.client.get_job_chunks_ready_for_client_processing_spectra_s3(
                ds3.GetJobChunksReadyForClientProcessingSpectraS3Request(self.jobid))
            #if self.debug:
            self.clientclass.dump_dict('ds3.GetJobChunksReadyForClientProcessingSpectraS3Request.json', chunkquery.result, 'file')
            available_chunks = chunkquery.result['ObjectsList']
            self.log.debug('Job ID "{}" has {} chunks on the Blackpearl for download.'.format(self.jobid, len(available_chunks)))
            if len(available_chunks) == 0:
                self.log.info('Waiting {} minutes before checking for more chunks to download.'.format(self.waitmin))
                time.sleep(self.waitmin*60)
            for chunk in available_chunks:
                self._download_chunk(chunk)
        if len(self.restorestatsclass.ChunksForDownload()) == len(self.restorestatsclass.ChunksDownloaded()):
            self.restorestatsclass.DownloadCompleteTime('set')
        return
    def _build_from_active_jobs(self):
        self.activejobsclass.build_active_jobs()
        if self.jobid not in self.activejobsclass.JobIds():
            raise Exception('Job ID "{}" not found in list of active job IDs.'.format(self.jobid))
        getjob = self.client.get_job_spectra_s3(ds3.GetJobSpectraS3Request(self.jobid))
        self.restorestatsclass.build_restore_stats(getjob.result)
        return
    def _download_chunk(self, chunk=None):
        if self.restorestatsclass.DownloadStartTime() == 'Not set': self.restorestatsclass.DownloadStartTime('set')
        chunkid = chunk['ChunkId']
        self.log.info('Downloading {:.3f} GB chunk "{}".'.format(float(self.restorestatsclass.ChunkSize(chunkid)/1073741824), chunkid))
        self.restorestatsclass.dowloaded_chunk(chunkid)
        for obj in chunk['ObjectList']:
            name = obj['Name']
            # Create a temporary file securely - tuple returned.
            prefix = obj['Id'] + '__'
            tfile = tempfile.mkstemp(prefix=prefix, dir=self.tempdir)
            if name not in list(self.tempfiles.keys()):
                self.tempfiles[name] = [ tfile ]
            else:
                self.tempfiles[name].append(tfile)
            self.log.info('Downloading {:.3f} GB sub-object "{}" as "{}".'.format(float(obj['Length'])/1073741824, obj['Id'], tfile[1]))
            object_stream = open(tfile[1], "wb")
            self.objclass.client.get_object(ds3.GetObjectRequest(
                self.objclass.BucketName(), 
                name, 
                object_stream, 
                offset=int(obj['Offset']), 
                job=self.jobid))
            os.close(tfile[0])
        return
    #
    #----------------------------------------------------------------------#
    def move_to_restoredir(self):
        for name in list(self.tempfiles.keys()):
            newname = self.restoredir + os.sep + os.path.basename(name)
            self.log.info('Concatenating {} downloaded chunks to "{}".'.format(len(self.tempfiles[name]), newname))
            outfile = open(newname, "w+b")
            for tfile in self.tempfiles[name]:
                self.log.info('"{}" --> "{}"'.format(tfile[1], newname))
                with open(tfile[1], "rb") as infile:
                    outfile.write(infile.read(self.memchunks))
                os.remove(tfile[1])
        self.restorestatsclass.RestoreTime('set')
        self.print_report()
        return
#
#=============================================================================#
