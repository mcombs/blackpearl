#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/jobstats.py
#=============================================================================#
#
import sys
import os
from . import log, objs, chunkstats
#
#=============================================================================#
class JobStats(object):
    '''Blackpearl job statistics.  This class calls chunkstats.ChunkStats().

    Attributes:
        debug        (OPT: bool)       : Enable debug mode.
        loglevel     (OPT: str)        : Log level.
        object_class (REQ: objs.Objs)  : Blackpearl object class.

    Methods:
        Status                 : Job status.
        RequestType            : Request type (GET, PUT, ...)
        StartDate              : Job start date.
        CachedSizeInBytes      : Bytes of object on cache.
        OriginalSizeInBytes    : Object size.
        ChunkIds               : Chunk IDs in this job.
        RunIds                 : RunIDs in this job.
        build_job_stats        : Build job stats from result of 
                                 ds3.get_bulk_job_spectra_s3()

        ChunkJobId             : Job ID for a chunk.
        ChunkRunIds            : Run IDs in a chunk.
        ChunkSubObjects        : List of sub-object IDs in a chunk.
        ChunkSize              : Chunk size.


    From ds3.get_bulk_job_spectra_s3(ds3.GetBulkJobSpectraS3Request()).result
    {
        "JobId": "43318f08-b366-4617-bee5-ae8d980e8d61",
        "Status": "IN_PROGRESS",
        "RequestType": "GET",
        "StartDate": "2019-06-03T20:43:30.000Z",
        "CachedSizeInBytes": "0",
        "OriginalSizeInBytes": "32705105920",
        ...
        "ObjectsList": [                                             <-- Plural
            {
                "ChunkNumber": "0",
                "ChunkId": "a297442f-e39a-4df9-8349-c0569028b05a",
                "ObjectList": [                                      <-- Singular
                    {
                        "InCache": "false",
                        "Latest": "true",
                        "Length": "68719476736",
                        "Id": "23857737-b648-41a8-98c6-65c5380e9313",
                        "VersionId": "06b99a6e-4487-4ab7-abf7-e1b8d392c648",
                        "Name": "invitae.komprise.com/kc/.86946/data/86935/run_data_11000__RU11845.tar",
                        "Bucket": null,
                        "Offset": "0",
                        "PhysicalPlacement": null
                    },
                    {
                        "InCache": "false",
                        "Latest": "true",
                        "Length": "68719476736",
                        "Id": "821b1f65-30e0-421d-8caa-e7c4f521c199",
                        "VersionId": "06b99a6e-4487-4ab7-abf7-e1b8d392c648",
                        "Name": "invitae.komprise.com/kc/.86946/data/86935/run_data_11000__RU11845.tar",
                        "Bucket": null,
                        "Offset": "68719476736",
                        "PhysicalPlacement": null
                    },
                ]
            }
        ],
    }

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal classes.
        self.objectclass = objs.Objs(debug=False, loglevel='WARNING')
        self.chunkstatsclass = chunkstats.ChunkStats(debug=self.debug, loglevel=self.loglevel)
        #
        # Internal variables.
        self.jobstats = {}
        return
    #----------------------------------------------------------------------#
    def JobStatus(self, jobid):              return self.jobstats[jobid]['Status']
    def JobRequestType(self, jobid):         return self.jobstats[jobid]['RequestType']
    def JobStartDate(self, jobid):           return self.jobstats[jobid]['StartDate']
    def JobCachedSizeInBytes(self, jobid):   return self.jobstats[jobid]['CachedSizeInBytes']
    def JobOriginalSizeInBytes(self, jobid): return self.jobstats[jobid]['OriginalSizeInBytes']
    def JobChunkIds(self, jobid):            return self.jobstats[jobid]['ChunkIds']
    def JobRunIds(self, jobid):              return self.jobstats[jobid]['RunIds']
    def init(self, jobid=None):
        jobid=str(jobid)
        if jobid in self.jobstats.keys():
            raise Exception('Data for job ID "{}" already exists.'.format(jobid))
        self.jobstats[jobid] = {
            'Status'               : '',
            'RequestType'          : '',
            'StartDate'            : '',
            'CachedSizeInBytes'    : 0,
            'OriginalSizeInBytes'  : 0,
            'ChunkIds'             : [],
            'RunIds'               : [],
        }
        return
    #----------------------------------------------------------------------#
    def build_job_stats(self, result=None):
        self.log.info('Building job statistics.')
        # There should be only one jobid generated for this get.
        jobid = str(result['JobId'])
        if jobid in self.jobstats.keys():
            raise Exception('Job ID "{}" already exists.'.format(jobid))
        self.init(jobid)
        self.jobstats[jobid]['Status']              = str(result['Status'])
        self.jobstats[jobid]['RequestType']         = str(result['RequestType'])
        self.jobstats[jobid]['StartDate']           = str(result['StartDate'])
        self.jobstats[jobid]['CachedSizeInBytes']   = int(result['CachedSizeInBytes'])
        self.jobstats[jobid]['OriginalSizeInBytes'] = int(result['OriginalSizeInBytes'])
        chunkids = []
        runids = []
        for chunk in result['ObjectsList']:
            chunkids.append(str(chunk['ChunkId']))
            for obj in chunk['ObjectList']:
                runid = self.objectclass.get_runid(str(obj['Name']))
                if runid not in runids: runids.append(runid)
        self.jobstats[jobid]['ChunkIds']            = chunkids
        self.jobstats[jobid]['RunIds']              = runids
        self.chunkstatsclass.build_chunk_stats(result)
        return jobid
    #----------------------------------------------------------------------#
    def JobPercentInCache(self, jobid):
        return float(self.JobCachedSizeInBytes(jobid)) / float(self.JobOriginalSizeInBytes(jobid)) * 100.000
    def ChunkJobId(self, chunkid):         return self.chunkstatsclass.ChunkJobId(chunkid)
    def ChunkRunIds(self, chunkid):        return self.chunkstatsclass.ChunkRunIds(chunkid)
    def ChunkSubObjects(self, chunkid):    return self.chunkstatsclass.ChunkSubObjects(chunkid)
    def ChunkSize(self, chunkid):          return self.chunkstatsclass.ChunkSize(chunkid)
    def SubChunkRunId(self, subchunkid):   return self.chunkstatsclass.SubChunkRunId(subchunkid)
    def SubChunkInCache(self, subchunkid): return bool(self.chunkstatsclass.SubChunkInCache(subchunkid))
    def SubChunkOffset(self, subchunkid):  return self.chunkstatsclass.SubChunkOffset(subchunkid)
    def SubChunkLength(self, subchunkid):  return self.chunkstatsclass.SubChunkLength(subchunkid)
#
#=============================================================================#
