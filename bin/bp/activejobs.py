#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/activejobs.py
#=============================================================================#
#
import sys
import os
from ds3 import ds3
from . import log, client, objs, jobstats
#
#=============================================================================#
class ActiveJobs(object):
    '''Blackpearl active jobs.

    Attributes:
        debug        (OPT: bool)          : Enable debug mode.
        loglevel     (OPT: str)           : Log level.

    Methods:
        CreatedAt              : Datetime job was created.
        RequestType            : Request type (GET, PUT, ...)
        CachedSizeInBytes      : Bytes of object on cache.
        OriginalSizeInBytes    : Object size.
        RunIds                 : RunIDs in this job.
        build_active_jobs      : Build internal data structure of jobs.
        GetJobs                : List of active GET jobs.
        PutJobs                : List of active PUT jobs.
        PercentInCache         : Percentage in cache.

    From ds3.get_active_jobs_spectra_s3(ds3.GetActiveJobsSpectraS3Request()).result
    {
        "JobList": [
            {
                "DeadJobCleanupAllowed": "true",
                "ErrorMessage": null,
                "Id": "dc3f4e21-7776-4fd8-bedd-f6cf46a2ba04",
                "CreatedAt": "2019-07-31T20:10:37.000Z",
                "UserId": "26b3e218-1a0c-4930-b23e-bcf27539ae14",
                "Rechunked": null,
                "BucketId": "09a02fd7-a3c7-4cd1-a8c2-279aca4f124a",
                "RequestType": "PUT",
                "Aggregating": "false",
                "ImplicitJobIdResolution": "false",
                "MinimizeSpanningAcrossMedia": "false",
                "TruncatedDueToTimeout": "false",
                "CompletedSizeInBytes": "0",
                "Restore": "NO",
                "Name": "PUT by 172.19.0.98",
                "Truncated": "false",
                "ChunkClientProcessingOrderGuarantee": "IN_ORDER",
                "VerifyAfterWrite": "false",
                "CachedSizeInBytes": "343597383680",
                "Replicating": "false",
                "Naked": "false",
                "Priority": "NORMAL",
                "OriginalSizeInBytes": "1318111068160"
            },
        ]
    }

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal classes.
        self.clientclass = client.Client(debug=self.debug, loglevel=self.loglevel)
        self.client = self.clientclass.connect()
        self.jobclass = jobstats.JobStats(debug=self.debug, loglevel=self.loglevel)
        #
        # Internal variables.
        self.activejobs = {}
        return
    #----------------------------------------------------------------------#
    def ActiveJobCreatedAt(self, jobid):         return self.activejobs[jobid]['CreatedAt']
    def init(self, jobid=None):
        jobid=str(jobid)
        if jobid in self.activejobs.keys():
            raise Exception('Data for job ID "{}" already exists.'.format(jobid))
        self.activejobs[jobid] = {
            'CreatedAt'            : '',
        }
        return
    #----------------------------------------------------------------------#
    def build_active_jobs(self):
        request = self.client.get_active_jobs_spectra_s3(ds3.GetActiveJobsSpectraS3Request())
        if self.debug:
            self.clientclass.dump_dict('ds3.GetActiveJobsSpectraS3Request.json', request.result)
        self._process_result(request.result)
        return
    def _process_result(self, result=None):
        for job in result['JobList']:
            jobid = str(job['Id'])
            self.init(jobid)
            self.activejobs[jobid]['CreatedAt'] = str(job['CreatedAt'])
            self._add_details(jobid)
        return
    def _add_details(self, jobid=None):
        jobinfo = self.client.get_job_spectra_s3(ds3.GetJobSpectraS3Request(jobid))
        if self.debug: 
            self.clientclass.dump_dict('ds3.GetJobSpectraS3Request.json', jobinfo.result)
        self.jobclass.build_job_stats(jobinfo.result)
        return
    #----------------------------------------------------------------------#
    def ActiveJobIds(self, jobtype=None):
        allowed_jobtypes = [ 'GET', 'PUT' ]
        if jobtype == None:
            return self.activejobs.keys()
        elif jobtype not in allowed_jobtypes:
            raise Exception('Unknown job type "{}". Allowed types "{}".'.format(jobtype, allowed_jobtypes))
        jobids = []
        for jobid in self.activejobs.keys():
            if self.RequestType(jobid) == jobtype: jobids.append(jobid)
        return jobids
    #----------------------------------------------------------------------#
    def JobRequestType(self, jobid):             return self.jobclass.JobRequestType(jobid)
    def JobCachedSizeInBytes(self, jobid):       return self.jobclass.JobCachedSizeInBytes(jobid)
    def JobOriginalSizeInBytes(self, jobid):     return self.jobclass.JobOriginalSizeInBytes(jobid)
    def JobRunIds(self, jobid):                  return self.jobclass.JobRunIds(jobid)
    def JobPercentInCache(self, jobid):          return self.jobclass.JobPercentInCache(jobid)
#
#=============================================================================#
