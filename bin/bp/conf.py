#!/usr/bin/env python3
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/conf.py
#=============================================================================#
#
import os
import sys
import configparser
from . import log
#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-3])
CREDSFILE = TOP_DIR + os.sep + 'etc' + os.sep + 'blackpearl-credentials.cfg'
#
#=============================================================================#
class Conf(object):
    '''Configuration.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        read_config            : Read the credentials file for connecting
                                 to the Blackpearl returning the dictionary.
    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        return
    def read_config(self):
        self.log.info('Reading config file "{}".'.format(CREDSFILE))
        cfg = configparser.RawConfigParser()
        cfg.read(CREDSFILE)
        config = { 
            'blackpearl_ip' : str(cfg['DEFAULT']['blackpearl_ip']),
            'access_key'    : str(cfg['DEFAULT']['access_key']),
            'secret_key'    : str(cfg['DEFAULT']['secret_key']),
        }
        return config
#=============================================================================#
