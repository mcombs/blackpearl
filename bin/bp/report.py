#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp/report.py
#=============================================================================#
#
import sys
import os
from . import log, timestats, objs, activejobs
#
#=============================================================================#
class Report(object):
    '''Blackpearl reporting methods.

    Attributes:
        debug    (OPT: bool)   : Enable debug mode.
        loglevel (OPT: str)    : Log level.

    Methods:
        method_name            : Method description.

    '''
    def __init__(self, debug=False, loglevel='INFO'):
        self.debug = debug
        if self.debug == True: loglevel='DEBUG'
        self.loglevel = loglevel.upper()
        mylog = log.Log(debug=self.debug, loglevel=self.loglevel)
        self.log = mylog.get()
        #
        # Internal classes.
        self.timeclass = timestats.TimeStats(debug=self.debug, loglevel=self.loglevel)
        #
        # Internal Variables
        self.gb = 1073741824

        return
    #----------------------------------------------------------------------#
    def _determine_tape_number(self, objectclass=None):
        max_tape_count = 0
        for runid in objectclass.ObjectRunIds():
            if len(objectclass.ObjectTapeInfo(runid)) > max_tape_count:
                max_tape_count = len(objectclass.ObjectTapeInfo(runid))
        return max_tape_count
    def objects_list(self, objectclass=None):
        output = 'RunID,Object,Size,Modified'
        n = 0
        while n < self._determine_tape_number(objectclass=objectclass):
            output += ',Tape_Serial,Tape_Barcode'
            n += 1
        output += '\n'
        for runid in sorted(objectclass.ObjectRunIds()):
            output += '{}'.format(runid)
            output += ',{}'.format(objectclass.ObjectBasename(runid))
            output += ',{}'.format(objectclass.ObjectSize(runid))
            output += ',"{}"'.format(objectclass.ObjectLastModified(runid))
            for t in objectclass.ObjectTapeInfo(runid):
                output += ',"{}","{}"'.format(t['SerialNumber'], t['BarCode'])
            output += '\n'
        return output
    #----------------------------------------------------------------------#
    def restore(self, jobid=None, restoreclass=None, gb=False):
        rpt = ''
        rpt += '{}\nRestore Report\n'.format('='*77)
        rpt += '\nTimes:\n'
        rpt += '  {:<30} : {}\n'.format('Report Timestamp', self.timeclass.utctime())
        rpt += '  {:<30} : {}\n'.format('Request Time', restoreclass.RequestTime())
        rpt += '  {:<30} : {}\n'.format('Download Start', restoreclass.DownloadStartTime())
        rpt += '  {:<30} : {}\n'.format('Download Complete', restoreclass.DownloadCompleteTime())
        rpt += '  {:<30} : {}\n'.format('Restore Complete', restoreclass.RestoreTime())
        rpt += '  {:<30} : {}\n'.format('Chunks for Download', restoreclass.ChunksForDownload())
        rpt += '  {:<30} : {}\n'.format('Chunks Downloaded', restoreclass.ChunksDownloaded())
        rpt += '\n'
        rpt += self.job(jobid, restoreclass, gb)
        return rpt
    #----------------------------------------------------------------------#
    def job_detail(self, jobid=None, jobclass=None, gb=False):
        rpt = ''
        rpt += '{}\nJob\n'.format('='*77)
        rpt += '  {:<30} : {}\n'.format('Job Id', jobid)
        rpt += '  {:<30} : {}\n'.format('Status', jobclass.JobStatus(jobid))
        rpt += '  {:<30} : {}\n'.format('Job Start', jobclass.JobStartDate(jobid))
        if gb:
            rpt += '  {:<30} : {:>15.3f}\n'.format('In Cache (GB)', jobclass.JobCachedSizeInBytes(jobid)/self.gb)
            rpt += '  {:<30} : {:>15.3f}\n'.format('Job Size (GB)', jobclass.JobOriginalSizeInBytes(jobid)/self.gb)
        else:
            rpt += '  {:<30} : {:>15}\n'.format('In Cache (Bytes)', jobclass.JobCachedSizeInBytes(jobid))
            rpt += '  {:<30} : {:>15}\n'.format('Job Size (Bytes)', jobclass.JobOriginalSizeInBytes(jobid))
        rpt += '  {:<30} : {}\n'.format('Run IDs', jobclass.JobRunIds(jobid))
        rpt += '\n{}\nChunks\n'.format('='*77)
        for chunkid in jobclass.JobChunkIds(jobid):
            rpt += '{:<30} : {}\n'.format('Chunk ID', chunkid)
            if gb:
                rpt += '{:<30} : {:>15.3f}\n'.format('Chunk Size (GB)', float(jobclass.ChunkSize(chunkid))/self.gb)
            else:
                rpt += '{:<30} : {:>15}\n'.format('Chunk Size (Bytes)', jobclass.ChunkSize(chunkid))
            rpt += '{:<30} : {}\n'.format('Run IDs', jobclass.ChunkRunIds(chunkid))
            rpt += '{:<30} : {}\n'.format('Sub-chunks', jobclass.ChunkSubObjects(chunkid))
            rpt += '{}\n\n'.format('-'*77)
        #
        rpt += '\n{}\nSubchunks\n'.format('='*77)
        if gb:
            rpt += '{:<40}  {:<10}  {:<10}  {:>15}  {:>15}\n'.format('ID', 'Run ID', 'In Cache', 'Offset (GB)', 'Length (GB)')
        else:
            rpt += '{:<40}  {:<10}  {:<10}  {:>15}  {:>15}\n'.format('ID', 'Run ID', 'In Cache', 'Offset (Bytes)', 'Length (Bytes)')
        rpt += '{:<40}  {:<10}  {:<10}  {:>15}  {:>15}\n'.format('-'*40, '-'*10, '-'*10, '-'*15, '-'*15)
        for chunkid in jobclass.JobChunkIds(jobid):
            for subchunkid in jobclass.ChunkSubObjects(chunkid):
                if gb:
                    rpt += '{:<40}  {:<10}  {:<10}  {:>15.3f}  {:>15.3}\n'.format(subchunkid,
                                                                              jobclass.SubChunkRunId(subchunkid),
                                                                              str(jobclass.SubChunkInCache(subchunkid)),
                                                                              float(jobclass.SubChunkOffset(subchunkid))/self.gb,
                                                                              float(jobclass.SubChunkLength(subchunkid))/self.gb
                                                                             )
                else:
                    rpt += '{:<40}  {:<10}  {:<10}  {:>15}  {:>15}\n'.format(subchunkid,
                                                                              jobclass.SubChunkRunId(subchunkid),
                                                                              str(jobclass.SubChunkInCache(subchunkid)),
                                                                              jobclass.SubChunkOffset(subchunkid),
                                                                              jobclass.SubChunkLength(subchunkid)
                                                                             )
        rpt += '{}\n'.format('='*77)
        return rpt
    #----------------------------------------------------------------------#
    def all_active_jobs(self, activejobclass=None, gb=False):
        output = 'Type,Job_ID,"Run_IDs","Created",Percent_in_Cache'
        if gb:
            output += ',"In_Cache (GB)","Size (GB)"\n'
        else:
            output += ',"In_Cache (bytes)","Size (bytes)"\n'
        for jobid in sorted(activejobclass.ActiveJobIds()):
            output += '{}'.format(activejobclass.JobRequestType(jobid))
            output += ',"{}"'.format(jobid)
            runidlist = ''
            for runid in activejobclass.JobRunIds(jobid):
                runidlist += '{} '.format(runid)
            output += ',"{}"'.format(runidlist[:-1])
            output += ',"{}"'.format(activejobclass.ActiveJobCreatedAt(jobid))
            output += ',{:.1f}'.format(activejobclass.JobPercentInCache(jobid))
            if gb == True:
                output += ',{:.3f}'.format(activejobclass.JobCachedSizeInBytes(jobid)/self.gb)
                output += ',{:.3f}'.format(activejobclass.JobOriginalSizeInBytes(jobid)/self.gb)
            else:
                output += ',{}'.format(activejobclass.JobCachedSizeInBytes(jobid))
                output += ',{}'.format(activejobclass.JobOriginalSizeInBytes(jobid))
            output += '\n'
        return output
    #----------------------------------------------------------------------#
    def active_job_log(self, jobid=None, activejobclass=None):
        rpt = ''
        rpt += '{}\nJob\n'.format('='*77)
        rpt += '    {:<25} : {}\n'.format('Type', activejobclass.JobRequestType(jobid))
        rpt += '    {:<25} : {}\n'.format('Job ID', jobid)
        runidlist = ''
        for runid in activejobclass.JobRunIds(jobid):
            runidlist += '{} '.format(runid)
        rpt += '    {:<25} : {}\n'.format('Run IDs', runidlist[:-1])
        rpt += '    {:<25} : {}\n'.format('Created At', activejobclass.ActiveJobCreatedAt(jobid))
        rpt += '{}\n'.format('='*77)
        return rpt
#
#=============================================================================#
