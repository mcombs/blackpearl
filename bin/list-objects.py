#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/list-objects.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
parser = argparse.ArgumentParser(description='List objects.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('--format', action='store', choices=['csv', 'json'], default='csv', help='Output format.')
parser.add_argument('--runids', action='store', nargs='*', default=None, help='RUs/RDs to list.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
objectclass = bp.objs.Objs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
reportclass = bp.report.Report(debug=args.debug, loglevel=args.loglevel)
objectclass.build_object_stats(wanted=args.runids)
#
if args.format == 'csv':
    print('{}'.format(reportclass.objects_list(objectclass)))
else:
    print('{}'.format(json.dumps(objectclass.stats(), indent=4)))
