#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/dump-job.py
#=============================================================================#
#
import sys
import os
import argparse
import bp
from ds3 import ds3
#
parser = argparse.ArgumentParser(description='Dump job information.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('jobids', action='store', nargs='+', default=None, help='Job IDs.')
args = parser.parse_args()
#
#
#-----------------------------------------------------------------------------#
# Set up classes and stats about active jobs.
clientclass = bp.client.Client(debug=True)
client = clientclass.connect()
#
js = bp.activejobs.ActiveJobs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
ajs.build_active_jobs()
#
#-----------------------------------------------------------------------------#
for jobid in args.jobids:
    verify = client.verify_bulk_job_spectra_s3(ds3.VerifyBulkJobSpectraS3Request('komprise_data', 
    clientclass.dump_dict('ds3.GetJobChunksReadyForClientProcessingSpectraS3Request.json', availableChunks.result)))
