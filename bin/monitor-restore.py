#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/monitor-restore.py
#=============================================================================#
#
import sys
import argparse
import time
import bp
parser = argparse.ArgumentParser(description='Monitor restore job.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('jobid', action='store', default=None, help='Restore job ID to monitor.')
args = parser.parse_args()
if args.debug == True: args.loglevel='DEBUG'
#
# Set up needed classes.
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
objectclass = bp.objs.Objs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
restclass = bp.restore.Restore(debug=args.debug, loglevel=args.loglevel, client_class=clientclass, object_class=objectclass)
#
restclass.GB(True)
restclass.monitor(jobid=args.jobid)
restclass.move_to_restoredir()
sys.exit(0)
