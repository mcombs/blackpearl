#!/usr/bin/env bash
#=============================================================================#
# Project Docs : 
# JIRA Ticket  : 
# Bitbucket    : https://bitbucket.org/mcombs/comp-projects/src/master/blackpearl/bin/samp-gettingData.py
#=============================================================================#
#
# This code was copied and altered to work as a test for the BlackPearl 
# API.
#
#   Copyright 2014-2017 Spectra Logic Corporation. All Rights Reserved.
#   Licensed under the Apache License, Version 2.0 (the "License"). You may not use
#   this file except in compliance with the License. A copy of the License is located at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#   or in the "license" file accompanying this file.
#   This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.

import os
import time
import tempfile
import json
from ds3 import ds3
from bp import session

def dump_dict(d, f):
    return
    print('Writing {}'.format(f))
    fd = open(f, 'w')
    fd.write(json.dumps(d, indent=4))
    fd.close()
client = ds3.createClientFromEnv()

bucketName = "komprise_data"
fileList = ["invitae.komprise.com/kc/.86946/data/86935/run_data__11000__11011.tar"]
bpsess = session.Session()

objectList = [ds3.Ds3GetObject(f) for f in fileList]
bulkGetResult = client.get_bulk_job_spectra_s3(ds3.GetBulkJobSpectraS3Request(bucketName, objectList))
bpsess.log_api_call(bulkGetResult)
dump_dict(bulkGetResult.result, 'GetBulkJobSpectraS3Request.json')

# create a set of the chunk ids which will be used to track
# what chunks have not been retrieved
chunkIds = set([x['ChunkId'] for x in bulkGetResult.result['ObjectsList']])

# create a dictionary to map our retrieved objects to temporary files
# if you want to keep the retreived files on disk, this is not necessary
tempFiles={}

# while we still have chunks to retrieve
while len(chunkIds) > 0:
    # get a list of the available chunks that we can get
    availableChunks = client.get_job_chunks_ready_for_client_processing_spectra_s3(
                             ds3.GetJobChunksReadyForClientProcessingSpectraS3Request(bulkGetResult.result['JobId']))
    bpsess.log_api_call(availableChunks)
    dump_dict(availableChunks.result, 'GetJobChunksReadyForClientProcessingSpectraS3Request.json')

    chunks = availableChunks.result['ObjectsList']

    # check to make sure we got some chunks, if we did not
    # sleep and retry.  This could mean that the cache is full
    if len(chunks) == 0:
        time.sleep(30)
        continue

    # for each chunk that is available, check to make sure
    # we have not gotten it, and if not, get that object
    for chunk in chunks:
        print('Getting chunk "{}".'.format(chunk))
        if not chunk['ChunkId'] in chunkIds:
            continue
        chunkIds.remove(chunk['ChunkId'])
        for obj in chunk['ObjectList']:
            # if we haven't create a temporary file for this object yet, create one
            if obj['Name'] not in list(tempFiles.keys()):
                tempFiles[obj['Name']]=tempfile.mkstemp(dir='/archive/bp_restore/tmp')
    
            # get the object
            objectStream = open(tempFiles[obj['Name']][1], "wb")
            client.get_object(ds3.GetObjectRequest(bucketName, 
                                                   obj['Name'],
                                                   objectStream,                                               
                                                   offset = int(obj['Offset']), 
                                                   job = bulkGetResult.result['JobId']))

# iterate over the temporary files, printing out their names, then closing and and removing them
for objName in list(tempFiles.keys()):
    print(objName)
    newname = '/archive/bp_restore/tmp/' + os.path.basename(objName)
    os.close(tempFiles[objName][0])
    os.rename(tempFiles[objName][1], newname)
    #os.remove(tempFiles[objName][1])


