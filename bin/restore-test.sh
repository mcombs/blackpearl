#!/usr/bin/env bash
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/restore-test.sh
#=============================================================================#


TOP_DIR="$(builtin cd $(dirname ${BASH_SOURCE[0]}) ; builtin cd .. ; builtin pwd)"
BIN_DIR="${TOP_DIR}/bin"
FNAME="${1}"


${BIN_DIR}/bp_retrieve_object.py ${FNAME}
${BIN_DIR}/compare-sums.sh ${FNAME}
