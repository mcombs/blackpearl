#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/find-duplicates.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
#-----------------------------------------------------------------------------#
# Parse arguments and set up logging.
parser = argparse.ArgumentParser(description='Find duplicate objects on the Blackpearl.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('--format', action='store', choices=['csv', 'json'], default='csv', help='Output format.')
parser.add_argument('--runids', action='store', nargs='*', default=None, help='RUs/RDs to list.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
#-----------------------------------------------------------------------------#
# Set up classes.
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
client = clientclass.connect()
#
# A few methods in object class are needed.
objs = bp.objs.Objs(debug=args.debug, loglevel=args.loglevel, client_class=clientclass)
#
#-----------------------------------------------------------------------------#
# Build our own list of object statistics.
objdict = {}
def get_objects(marker=None):
    (retrieved, initialized) = (0,0)
    bucket_contents = client.get_bucket(ds3.GetBucketRequest(objs.BucketName(), marker=marker))
    for objects in bucket_contents.result['ContentsList']:
        object_name = objects['Key']
        retrieved += 1
        if not objs.is_ru(object_name): continue
        runid = objs.get_runid(object_name)
        mydict = { 
            'ObjectName'      : object_name,
            'Size'            : int(objects['Size']),
            'LastModified'    : str(objects['LastModified']),
        }
        if runid not in objdict.keys(): objdict[runid] = []
        objdict[runid].append(mydict)
        initialized += 1
    return (bucket_contents.result['Marker'],bucket_contents.result['NextMarker'], retrieved, initialized)
def build_object_stats():
    (total_retrieved, total_initialized) = (0, 0)
    (marker, nextmarker, retrieved, initialized) = get_objects(marker=None)
    total_retrieved += retrieved
    total_initialized += initialized
    log.info('Initialized {} of {} objects retrieved.'.format(total_initialized, total_retrieved))
    while nextmarker:
        (marker, nextmarker, retrieved, initialized) = get_objects(nextmarker)
        total_retrieved += retrieved
        total_initialized += initialized
        log.info('Initialized {} of {} objects retrieved.'.format(total_initialized, total_retrieved))
    return
build_object_stats()
#
#-----------------------------------------------------------------------------#
# Pull out any duplicates.
dupes_found = False
output = '{},{},{},{}\n'.format('RunId', 'Size', 'LastModified', 'Object')
output += '{},{},{},{}\n'.format('='*10, '='*21, '='*25, '='*40)
for runid in objdict.keys():
    if len(objdict[runid]) == 1: continue
    dupes_found = True
    for dupe in objdict[runid]:
        output += '{},{},{},{}\n'.format(runid, dupe['Size'], dupe['LastModified'], dupe['ObjectName'])
    output += '{},{},{},{}\n'.format('-'*10, '-'*21, '-'*25, '-'*40)
if dupes_found == True:
    print('{}'.format(output))
