#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/comp-projects/src/master/blackpearl/bin/setenv.sh
#=============================================================================#
#
# Run this to set environment variables if using the ds3.createClientFromEnv()
# class to authenticate against the Blackpearl.
#
export DS3_ENDPOINT="172.19.0.56"
export DS3_ACCESS_KEY="**REDACTED**"
export DS3_SECRET_KEY="**REDACTED**"
