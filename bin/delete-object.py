#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/delete-object.py
#=============================================================================#
#
import sys
import argparse
import json
import bp
from ds3 import ds3
#
#-----------------------------------------------------------------------------#
# Parse arguments and set up logging.
parser = argparse.ArgumentParser(description='Delete Blackpearl object.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='INFO', help='Log level.')
parser.add_argument('bucket_name', action='store', default=None, help='Bucket name.')
parser.add_argument('object_name', action='store', default=None, help='Full path of object to delete.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
#-----------------------------------------------------------------------------#
# Set up classes and stats about active jobs.
clientclass = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
client = clientclass.connect()
#
#-----------------------------------------------------------------------------#
def proceed(objectname=None):
    valid = {'YES': True, 'NO': False, 'N': False}
    while True:
        sys.stdout.write('Delete object\n    {}\n    [YES / NO] (Default: NO) : '.format(objectname))
        choice = raw_input()
        if choice == '':
            return False
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write('Please respond with "YES" or "NO" ')
    return False
#
if proceed(object_name):
    log.warning('Deleting object "{}" from bucket "{}".'.format(args.object_name, args.bucket_name))
    response = client.delete_object(ds3.DeleteObjectRequest(args.bucket_name, args.object_name))
sys.exit()
