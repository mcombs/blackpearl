#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/samp-test-bp.py
#=============================================================================#
#
import os
import sys
import json
import re
import argparse
import logging
import bp
from ds3 import ds3
#
#
#=============================================================================#
TOP_DIR = str(os.sep).join(os.path.realpath(__file__).split(os.sep)[:-2])
#
#
#=============================================================================#
def main():
    parser = argparse.ArgumentParser(description='Connect and list objects on the Blackpearl.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--debug', action='store_true', default=False, help='Enable debug mode.')
    parser.add_argument('--loglevel', action='store', default='WARNING', help='Log level.')
    parser.add_argument('--listobj', action='store_true', default=False, help='List all bucket objects rather than count.')
    args = parser.parse_args()
    s = SpectraTest(args=args)
    s.init_logging(loglevel=args.loglevel)
    s.setup()
    s.print_bucket_contents()
    return(s.exit_status)
#
#
#=============================================================================#
class SpectraTest(object):
    '''
    '''
    #
    #
    #-------------------------------------------------------------------------#
    def __init__(self, args=None):
        self.exit_status = 0
        self.debug = args.debug
        self.loglevel = args.loglevel.upper()
        self.listobj = args.listobj
        #
        # Internal variables.
        self.client = None
        self.rundataconf = bp.rundataconf.RunDataConf(debug=self.debug, loglevel=self.loglevel)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def init_logging(self, loglevel=None):
        '''Assumes loglevel = 'critical|error|warning|info|debug'
        '''
        levels = [ 'CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG' ]
        if loglevel.upper() not in levels:
            loglevel='WARNING'
        log = logging.getLogger(__name__)
        formatter_basic = logging.Formatter(fmt='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
        formatter_debug = logging.Formatter(fmt='DEBUG: [%(asctime)s] %(filename)s(%(process)d) %(levelname)s [%(name)s.%(funcName)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S %z')
        sh = logging.StreamHandler(stream=sys.stderr)
        if self.debug:
            loglevel='DEBUG'
            sh.setFormatter(formatter_debug)
        else:
            loglevel = loglevel.upper()
            sh.setFormatter(formatter_basic)
        log.setLevel(getattr(logging, loglevel))
        log.addHandler(sh)
        self.log = log
        return
    #
    #
    #-------------------------------------------------------------------------#
    def setup(self):
        '''Set up dependencies before running.  This should be run AFTER
logging has been set up.
        '''
        self.log.info('Setting up.')
        client = bp.client.Client(debug=self.debug, logger=self.log)
        self.client = client.connect()
        objstats = bp.objstats.ObjStats(debug=self.debug, logger=self.log, client=self.client)
        return
    #
    #
    #-------------------------------------------------------------------------#
    def print_bucket_contents(self):
        '''Print bucket contents.
        '''
        self.log.debug('Getting contents of bucket "{}".'.format(self.cfg['bucket_name']))
        bucket_contents = self.client.get_bucket(ds3.GetBucketRequest(self.cfg['bucket_name']))
        object_names = [bucket['Key'] for bucket in bucket_contents.result['ContentsList']]
        if self.listobj == False:
            print('{}\nFound {} objects for bucket : {}'.format('='*77, len(object_names), self.cfg['bucket_name']))
        else:
            print('{}\nObjects for bucket : {}'.format('='*77, self.cfg['bucket_name']))
            for name in object_names:
                 print('    {}'.format(name))
#
#
#=============================================================================#
if __name__ == "__main__": sys.exit(main())

