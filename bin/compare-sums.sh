#!/usr/bin/env bash
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/compare-sums.sh
#=============================================================================#
#
#
INAME="${1%%.tar}"
ORIG_SUM="$(find /archive/run_data_staging/meta -iname "*${INAME}*" | xargs grep '"file_checksum"' | sed -e 's/.*://; s/",//; s/"//g; s/ //g')"
CURR_SUM="$(find /archive/bp_restore -iname "*${INAME}*" | xargs -I {} sha512sum {} | awk '{ print $1 }' | sed -e 's/ //g')"
cat <<EOF
SHA512 Checksums:
Original metadata file : ${ORIG_SUM}
Restored file          : ${CURR_SUM}
EOF
