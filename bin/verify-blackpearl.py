#!/usr/bin/env python3
#=============================================================================#
# Project Docs :
# JIRA Ticket  :
# Bitbucket    : https://bitbucket.org/mcombs/blackpearl/src/master/bin/bp-list-tapes.py
#=============================================================================#
#
import sys
import argparse
import bp
parser = argparse.ArgumentParser(description='Test connectivity to the Blackpearl.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--debug', action='store_true', default=True, help='Enable debug mode.')
parser.add_argument('--loglevel', action='store', default='DEBUG', help='Log level.')
args = parser.parse_args()
#
if args.debug == True: args.loglevel = 'DEBUG'
logclass = bp.log.Log(debug=args.debug, loglevel=args.loglevel)
log = logclass.get()
#
client = bp.client.Client(debug=args.debug, loglevel=args.loglevel)
myclient = client.connect()
print('Obtained client : {}'.format(myclient))
