# blackpearl

Code for connecting to the Blackpearl API.

# Processes
Prepare the environment.
* Extract the `ds3` directory from the python Blackpearl API tar archive to `./bin/bp/ds3`
* Symlink `./bin/ds3` to `./bin/bp/ds3`
* Update the credentials file - `./etc/blackpearl-credentials.cfg` - with the access key and secret key.
* Create the python virtual env
    * `cd ./bin`
    * `make ve`
    * `source ./ve/bin/activate`
* Test connectivity to the Blackpearl using `./bin/verify-blackpearl.py`

# Code
## Setup / General
* `Makefile` - Makefile for building/destroying the python virtualenv environment.
* `delete-object.py` - Delete the object on the Blackpearl.
* `find-duplicates.py` - Find duplicate objects on the Blackpearl.
* `list-objects.py` - List objects on the Blackpearl/DS3 tape storage.
* `samp-gettingData.py` - Sample from Spectra on which object retrieval is based.
* `samp-listAll.py` - Sample from Spectra on which object listing is based.
* `samp-test-bp.py` - Initial monolithic script to list objects on the Blackpearl.
* `setenv.sh` - Set environment variables in lieu of reading a config file for Blackpearl credentials.

## Jobs
* `cancel-activejob.py` - Cancel and active GET/PUSH job on the Blackpearl.
* `list-active-jobs.py` - List active jobs on the Blackpearl.
* `view-job.py` - View details of a GET/PUSH job.

## Restore (GET) jobs.
* `cancel-get-jobs.py` - Cancel all GET jobs on the Blackpearl.
* `monitor-restore.py` - Monitor running restore jobs on the Blackpearl.
* `restore-runs.py` - Restore specified run IDs.


# Hacky additions
* `compare-sums.sh` - Compare checksums.
* `dump_job.py` - Dump JSON formatted job data.
* `restore-test.sh` - Rudimentary wrapper for restoring and comparing checksums.

